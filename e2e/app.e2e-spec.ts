import { EdAnalyticsV2Page } from './app.po';

describe('ed-analytics-v2 App', function() {
  let page: EdAnalyticsV2Page;

  beforeEach(() => {
    page = new EdAnalyticsV2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
